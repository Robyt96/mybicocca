<html>
  <head>
    <title>MyBicocca</title>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script>
      function searchPhp() {
        $.post( "search.php", $('#theForm').serialize(), function( data ) {
            $( "#risultati" ).html( data );
        });
      }
    </script>
  </head>
  <body style='font-family:Arial;font-size:12px;'>
     <h1>MyBicocca</h1>
     <h3>Ricerca professori / edifici Università Bicocca:</h3>
     <form id="theForm" onsubmit="return false;">
     <table cellspacing='0' cellpadding='5' border='1'>
       <tr><td>Cognome</td><td><input type="text" name="cognome" id="cognome" /></td></tr>
       <tr><td>Nome</td><td><input type="text" name="nome" id="nome" /></td></tr>
       <tr><td>Edificio</td><td><input type="text" name="edificio" id="edificio" /></td></tr>
       <tr><td colspan='2' align='right'><input type="button" value="Cerca" onclick="searchPhp( );" /></td></tr>
     </table>
     </form>
     <div id="risultati" style='margin-top:40px;'></div>
  </body>
</html>