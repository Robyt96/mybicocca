<?php
  //open connection
  $servername = "mysql";
  $username = "mybicoccauser";
  $password = "bicocca2018";
  $dbname = "mybicoccadb";

  $conn = new mysqli($servername, $username, $password, $dbname);
    
  $cognome = mysqli_real_escape_string($conn, $_POST["cognome"]);
  $nome = mysqli_real_escape_string($conn, $_POST["nome"]);
  $edificio = mysqli_real_escape_string($conn, $_POST["edificio"]);
  
  //query
  $where = '';
  if ($cognome <> '') $where .= "and cognome='$cognome' ";
  if ($nome <> '') $where .= "and p.nome='$nome' ";
  if ($edificio <> '') $where .= "and u.nome='$edificio' ";
  
  $sql = "select u.nome edificio, u.dipartimento, u.citta, u.indirizzo, p.piano, p.stanza,"
    . "p.cognome, p.nome, p.ruolo, p.disciplina, p.telefono, p.mail "
    . "from edifici u left join professori p on u.nome = p.edificio "
    . "where 1=1 $where"
    . "order by u.nome, p.cognome, p.nome";
  
  $result = $conn->query($sql);
  
  //results
  if ($result->num_rows > 0) {
    print "<table cellspacing='0' cellpadding='5' border='1'>\n";
    $r = 0;

    while($row = $result->fetch_assoc()) {
        if ($r++ == 0) {
            print "<tr>";
            foreach($row as $key=>$value) {
              print  "<th>" . $key . "</th>";
            }
            print "</tr>";            
        }
        
        print "<tr>";
        foreach($row as $key=>$value) {
          print  "<td>" . $value . "</td>";
        }
        print "</tr>";
    }

    print "</table>\n";
  } else {
    echo "<h4>Nessun risultato</h4>";
  }  

  //close connection
  $conn->close();
?>