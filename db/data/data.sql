-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: mybicoccadb
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `edifici`
--

DROP TABLE IF EXISTS `edifici`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edifici` (
  `nome` varchar(4) NOT NULL,
  `dipartimento` varchar(50) DEFAULT NULL,
  `citta` varchar(30) DEFAULT NULL,
  `indirizzo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edifici`
--

LOCK TABLES `edifici` WRITE;
/*!40000 ALTER TABLE `edifici` DISABLE KEYS */;
INSERT INTO `edifici` VALUES ('U02','Fisica','Milano','Piazza della Scienza, 3'),('U03','Biotecnologie e Bioscienze','Milano','Piazza della Scienza, 2'),('U05','Matematica','Milano','Via Roberto Cozzi, 55'),('U07','Economia, Scienze Statistiche, Sociologia','Milano','Via Bicocca degli Arcimboldi, 8'),('U14','Informatica, Sistemistica e Comunicazione','Milano','Viale Sarca, 336'),('U18','Polo di Medicina','Vedano al Lambro','Via Podgora');
/*!40000 ALTER TABLE `edifici` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professori`
--

DROP TABLE IF EXISTS `professori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cognome` varchar(50) DEFAULT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `ruolo` varchar(50) DEFAULT NULL,
  `disciplina` varchar(100) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `edificio` varchar(4) DEFAULT NULL,
  `piano` varchar(10) DEFAULT NULL,
  `stanza` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `edificio` (`edificio`),
  KEY `professori_idx1` (`cognome`,`nome`),
  CONSTRAINT `professori_ibfk_1` FOREIGN KEY (`edificio`) REFERENCES `edifici` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professori`
--

LOCK TABLES `professori` WRITE;
/*!40000 ALTER TABLE `professori` DISABLE KEYS */;
INSERT INTO `professori` VALUES (1,'Mariani','Leonardo','professore ordinario','SISTEMI DI ELABORAZIONE DELLE INFORMAZIONI','0264487870','leonardo.mariani@unimib.it','U14','P02','2039'),(2,'Arrigo','Ugo','professore associato','SCIENZA DELLE FINANZE','0264483087','ugo.arrigo@unimib.it','U07','P03','3010l'),(4,'Nucciotti','Angelo Enrico Lodovico','professore associato','FISICA NUCLEARE E SUBNUCLEARE','0264482428','angelo.nucciotti@unimib.it','U02','P04','4028'),(5,'Schettini','Raimondo','professore ordinario','INFORMATICA','0264487840','raimondo.schettini@unimib.it','U14','P02','2059'),(6,'Gasparini','Francesca','professore associato','INFORMATICA','0264487856','francesca.gasparini@unimib.it','U14','P01','1012'),(7,'De Gioia','Luca','professore ordinario','CHIMICA GENERALE E INORGANICA','0264483475','luca.degioia@unimib.it','U03','P03','3063'),(8,'Coccetti','Paola','ricercatore','BIOCHIMICA','0264483521','paola.coccetti@unimib.it','U03','P05','5011'),(9,'Moscato','Ugo Emanuele','professore ordinario','INFORMATICA','0264483134','ugo.moscato@unimib.it','U07','P04','4128'),(10,'Fontana','Luigi','ricercatore','ANALISI MATEMATICA','0264485713','luigi.fontana@unimib.it','U05','P03','3064'),(11,'Ferretti','Claudio','professore associato','INFORMATICA','0264487819','claudio.ferretti@unimib.it','U14','P02','2053'),(12,'Bonizzoni','Paola','professore ordinario','INFORMATICA','0264487814','paola.bonizzoni@unimib.it','U14','P02','2035');
/*!40000 ALTER TABLE `professori` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-29 21:09:21
