create table edifici (
  nome varchar(4) primary key,
  dipartimento varchar(50),
  citta varchar(30),
  indirizzo varchar(100)
);

create table professori (
  id int auto_increment primary key,
  cognome varchar(50),
  nome varchar(50),
  ruolo varchar(50),
  disciplina varchar(100),
  telefono varchar(20),
  mail varchar(50),
  edificio varchar(4),
  piano varchar(10),
  stanza varchar(10)
);

ALTER TABLE professori
ADD FOREIGN KEY (edificio)
REFERENCES edifici(nome);

create index professori_idx1 on professori(cognome, nome);